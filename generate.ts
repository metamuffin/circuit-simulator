import { readFileSync, writeFileSync } from "fs";
import { join } from "path/posix";

export interface Node {
    kind: "and" | "or" | "xor" | "not" | "input" | "output" | "buffer"
    id: string
    inputs: string[]
}

if (process.argv.length < 4) throw new Error("AAA");
const input_file = process.argv[2]
const output_file = process.argv[3]

const { nodes, outputs, inputs }: { nodes: Node[], outputs: string[], inputs: string[] } = JSON.parse(readFileSync(input_file).toString())

let nodes_map: Map<string, Node> = new Map()
nodes.forEach(n => nodes_map.set(n.id, n))

let nodes_ordered: Node[] = []

const add_node = (id: string) => {
    if (inputs.includes(id)) return
    const n = nodes_map.get(id)
    if (!n) throw new Error("AAAAAAAAAAAAAA");
    if (nodes_ordered.find(no => no.id == id)) return;
    if (n.kind != "buffer")
        n.inputs.forEach(ni => add_node(ni))
    nodes_ordered.push(n)
}
outputs.forEach(add_node)

let step_code = ""
let global_code = ""
let post_step_code = ""
let input_code = ""
let output_code = ""


for (const n of nodes_ordered) {
    if (n.kind == "and")
        step_code += `char ${n.id} = ${n.inputs[0]} && ${n.inputs[1]};\n`
    if (n.kind == "or")
        step_code += `char ${n.id} = ${n.inputs[0]} || ${n.inputs[1]};\n`
    if (n.kind == "xor")
        step_code += `char ${n.id} = ${n.inputs[0]} != ${n.inputs[1]};\n`
    if (n.kind == "not")
        step_code += `char ${n.id} = !${n.inputs[0]};\n`
}
for (const n of nodes_ordered) {
    if (n.kind != "buffer") continue
    global_code += `char ${n.id};\n`
    post_step_code += `${n.id} = ${n.inputs[0]};\n`
}

for (const i of inputs) {
    global_code += `char ${i};\n`
    input_code += `${i} = getchar() == '0';\n`
}
input_code += `getchar();` // read newline character

for (const o of outputs) {
    output_code += `printf("%i", ${o});\n`
}


let code = `
#include "stdio.h"
${global_code.trim()}
void step() {
${input_code}
${step_code.trim()}
${post_step_code.trim()}
${output_code}    
}
int main() {
    while(1) {
        step();
    }
}
`

writeFileSync(output_file, code)
