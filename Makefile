
generate.js: generate.ts
	tsc generate.ts

simulate.c: circuit.json generate.js
	node generate.js circuit.json simulate.c

simulate: simulate.c
	gcc -o simulate simulate.c


